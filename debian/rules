#!/usr/bin/make -f
# -*- makefile -*-
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

%:
	dh $@ --with autoreconf

PKD  = $(abspath $(dir $(MAKEFILE_LIST)))
PKG  = $(word 2,$(shell dpkg-parsechangelog -l$(PKD)/changelog | grep ^Source))
VER  = $(shell dpkg-parsechangelog -l$(PKD)/changelog | perl -ne 'print $$1 if m{^Version:\s+(?:\d+:)?(\d.*)(?:\-\d+.*)};')

## http://wiki.debian.org/onlyjob/get-orig-source
.PHONY: get-orig-source
get-orig-source: $(PKG)_$(VER)$(DTYPE).orig.tar.xz $(info I: $(PKG)_$(VER)$(DTYPE))
	@

UURL = git://github.com/neubig/travatar.git
UDATE = $(shell date --rfc-3339=seconds --date='TZ="UTC" $(shell echo $(VER) | perl -ne 'print "$$1-$$2-$$3" if m/\+(?:git|svn|hg)(\d{4})(\d{2})(\d{2})/')')
$(PKG)_$(VER)$(DTYPE).orig.tar.xz: $(info I: UDATE=$(UDATE))
	$(if $(wildcard $(PKG)-$(VER)),$(error $(PKG)-$(VER) exist, aborting..))
	@echo "# Downloading..."
	git clone $(UURL) $(PKG)-$(VER) \
	|| $(RM) -r $(PKG)-$(VER)
	cd $(PKG)-$(VER) \
	&& git checkout v$(VER) || git checkout $$(git log -n1 --format=%h --before="$(UDATE)") \
	&& [ -s ChangeLog ] || ( echo "# Generating ChangeLog..." \
	   ; git log --pretty="format:%ad  %aN  <%aE>%n%n%x09* %s%n" --date=short > ChangeLog \
	   ; touch -d "$$(git log -1 --format='%ci')" ChangeLog) \
	&& echo "# Setting times..." \
	&& for F in $$(git ls-tree -r --name-only HEAD); do touch --no-dereference -d "$$(git log -1 --format="%ai" -- $$F)" "$$F"; done \
	&& echo "# Cleaning-up..." \
	&& $(RM) -r -v \
	    src/liblbfgs/AUTHORS \
	    src/liblbfgs/ChangeLog \
	    src/liblbfgs/Makefile.am \
	    src/liblbfgs/NEWS \
	    src/liblbfgs/README \
	    src/liblbfgs/arithmetic_ansi.h \
	    src/liblbfgs/arithmetic_sse_double.h \
	    src/liblbfgs/arithmetic_sse_float.h \
	    src/liblbfgs/lbfgs.c \
	    src/liblbfgs/lbfgs.h \
	    src/marisa \
	    src/kenlm \
	    src/tercpp \
	&& $(RM) -r .git .git*
	@echo "# Packing..."
	find -L "$(PKG)-$(VER)" -xdev -type f -print | sort \
	| XZ_OPT="-6v" tar -caf "$(PKG)_$(VER)$(DTYPE).orig.tar.xz" -T- --owner=root --group=root --mode=a+rX \
	&& $(RM) -r "$(PKG)-$(VER)"
